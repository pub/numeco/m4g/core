<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.0.7</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>

	<groupId>org.mte.numecoeval</groupId>
	<artifactId>core</artifactId>
	<version>1.0.0</version>
	<name>core</name>
	<packaging>pom</packaging>
	<description>Projet parent du projet NumEcoEval</description>

	<repositories>
		<repository>
			<id>gitlab-maven</id>
			<url>https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/groups/5389/-/packages/maven</url>
		</repository>
	</repositories>
	<distributionManagement>
		<repository>
			<id>gitlab-maven</id>
			<url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
		</repository>
		<snapshotRepository>
			<id>gitlab-maven</id>
			<url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
		</snapshotRepository>
	</distributionManagement>

	<properties>
		<java.version>17</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<!-- Utilitaires -->
		<apache-commons-lang3.version>3.12.0</apache-commons-lang3.version>
		<apache-commons-collection4.version>4.4</apache-commons-collection4.version>
		<apache-commons-io.version>2.11.0</apache-commons-io.version>
		<apache-commons-csv.version>1.9.0</apache-commons-csv.version>
		<mapstruct.version>1.5.3.Final</mapstruct.version>

		<!-- CVE multiples sur Snakeyaml (CVE-2020-13936, CVE-2022-38749, CVE-2022-38751, CVE-2022-38750, CVE-2022-38752)-->
		<snakeyaml.version>1.33</snakeyaml.version>
		<!-- CVE sur Postgres (CVE-2022-41946) -->
		<postgresql.version>42.5.4</postgresql.version>

		<!-- CVE sur Spring CVE-2023-20863-->
		<spring-framework.version>6.0.8</spring-framework.version>

		<!-- Documentation automatique des API REST en OpenAPI 3.0 -->
		<springdoc-openapi-ui.version>2.0.4</springdoc-openapi-ui.version>

		<!-- OpenAPI Generator (API REST generator) -->
		<openapi-generator-version>6.4.0</openapi-generator-version>
		<jackson-databind-nullable-version>0.2.6</jackson-databind-nullable-version>
		<!-- scs-multiapi-maven-plugin (Async API generator)-->
		<scs-multiapi-maven-plugin.version>4.5.0</scs-multiapi-maven-plugin.version>

		<!-- Tests -->
		<meanbean.version>2.0.3</meanbean.version>
		<equalsverifier.version>3.10.1</equalsverifier.version>
		<wiremock.version>2.34.0</wiremock.version>
		<javafaker.version>1.0.2</javafaker.version>
		<cucumber-bom.version>7.11.1</cucumber-bom.version>
		<jacoco.version>0.8.8</jacoco.version>
		<instancio-junit.version>2.2.0</instancio-junit.version>
		<surefireArgLine>-Dfile.encoding=UTF-8</surefireArgLine>
		<embedded-database-spring-test.version>2.2.0</embedded-database-spring-test.version>
		<embedded-postgres.version>2.0.3</embedded-postgres.version>
	</properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.mte.numecoeval</groupId>
                <artifactId>common</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>org.meanbean</groupId>
                <artifactId>meanbean</artifactId>
                <version>${meanbean.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons-lang3.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-collections4</artifactId>
                <version>${apache-commons-collection4.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>${apache-commons-io.version}</version>
            </dependency>

			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-csv</artifactId>
				<version>${apache-commons-csv.version}</version>
			</dependency>

			<dependency>
				<groupId>org.mapstruct</groupId>
				<artifactId>mapstruct</artifactId>
				<version>${mapstruct.version}</version>
			</dependency>

			<dependency>
				<groupId>org.springdoc</groupId>
				<artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
				<version>${springdoc-openapi-ui.version}</version>
			</dependency>

			<dependency>
				<groupId>org.springdoc</groupId>
				<artifactId>springdoc-openapi-starter-common</artifactId>
				<version>${springdoc-openapi-ui.version}</version>
			</dependency>

			<!-- OpenAPI Generator -->
			<dependency>
				<groupId>org.openapitools</groupId>
				<artifactId>jackson-databind-nullable</artifactId>
				<version>${jackson-databind-nullable-version}</version>
			</dependency>

			<!-- Cucumber (Tests) -->
			<dependency>
				<groupId>io.cucumber</groupId>
				<artifactId>cucumber-bom</artifactId>
				<version>${cucumber-bom.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<dependency>
				<groupId>nl.jqno.equalsverifier</groupId>
				<artifactId>equalsverifier</artifactId>
				<version>${equalsverifier.version}</version>
				<scope>test</scope>
			</dependency>


			<!-- Donnnées aléatoires -->
			<dependency>
				<groupId>com.github.javafaker</groupId>
				<artifactId>javafaker</artifactId>
				<version>${javafaker.version}</version>
				<scope>test</scope>
			</dependency>

			<dependency>
				<groupId>org.instancio</groupId>
				<artifactId>instancio-junit</artifactId>
				<version>${instancio-junit.version}</version>
				<scope>test</scope>
			</dependency>
			<!-- Wiremock-->
			<dependency>
				<groupId>com.github.tomakehurst</groupId>
				<artifactId>wiremock-jre8-standalone</artifactId>
				<version>${wiremock.version}</version>
				<scope>test</scope>
				<exclusions>
					<exclusion>
						<groupId>junit</groupId>
						<artifactId>junit</artifactId>
					</exclusion>
				</exclusions>
			</dependency>

			<!-- Base de données de tests-->
			<dependency>
				<groupId>io.zonky.test</groupId>
				<artifactId>embedded-database-spring-test</artifactId>
				<version>${embedded-database-spring-test.version}</version>
				<scope>test</scope>
			</dependency>
			<dependency>
				<groupId>io.zonky.test</groupId>
				<artifactId>embedded-postgres</artifactId>
				<version>${embedded-postgres.version}</version>
				<scope>test</scope>
			</dependency>

        </dependencies>
    </dependencyManagement>

	<profiles>
		<profile>
			<id>FULL</id>
			<modules>
				<module>../common</module>
				<module>../common-dto</module>
				<module>../calculs</module>
				<module>../api-rest-calculs</module>
				<module>../api-referentiel</module>
				<module>../api-expositiondonneesentrees</module>
				<module>../api-event-donneesEntrees</module>
				<module>../api-event-enrichissement</module>
				<module>../api-event-calculs</module>
				<module>../api-event-indicateurs</module>
			</modules>
		</profile>
		<!-- Dependency Check en local-->
		<profile>
			<id>DEPENDENCY-CHECK</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.owasp</groupId>
						<artifactId>dependency-check-maven</artifactId>
						<version>8.1.2</version>
						<executions>
							<execution>
								<goals>
									<goal>check</goal>
								</goals>
								<configuration>
									<suppressionFile>${project.basedir}/dependency_check_suppressions.xml</suppressionFile>
								</configuration>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		<!--	Bypass des tests	-->
		<profile>
			<id>SKIP-ALL-TEST</id>
			<properties>
				<skip.unit.tests>true</skip.unit.tests>
				<skip.integration.tests>true</skip.integration.tests>
				<maven.test.failure.ignore>true</maven.test.failure.ignore>
			</properties>
		</profile>
	</profiles>

	<build>


		<plugins>
			<!-- Processors pour les annotations -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.1</version>
				<configuration>
					<source>${java.version}</source> <!-- depending on your project -->
					<target>${java.version}</target> <!-- depending on your project -->
					<encoding>${project.build.sourceEncoding}</encoding>
					<annotationProcessorPaths>
						<path>
							<groupId>org.mapstruct</groupId>
							<artifactId>mapstruct-processor</artifactId>
							<version>${mapstruct.version}</version>
						</path>
						<path>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok</artifactId>
							<version>${lombok.version}</version>
						</path>
						<path>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok-mapstruct-binding</artifactId>
							<version>0.2.0</version>
						</path>
						<path>
							<groupId>com.github.therapi</groupId>
							<artifactId>therapi-runtime-javadoc-scribe</artifactId>
							<version>0.15.0</version>
						</path>
						<path>
							<groupId>org.instancio</groupId>
							<artifactId>instancio-processor</artifactId>
							<version>2.2.0</version>
						</path>
						<!-- other annotation processors -->
					</annotationProcessorPaths>
				</configuration>
			</plugin>

			<!-- Correction nécessaire pour la gestion de l'UTF-8 sous Windows -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<redirectTestOutputToFile>true</redirectTestOutputToFile>
					<argLine>@{surefireArgLine}</argLine>
				</configuration>
			</plugin>

			<!-- Plugin pour le reporting de la couverture du code -->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>${jacoco.version}</version>
				<executions>
					<execution>
						<id>pre-unit-test</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
						<configuration>
							<!-- Sets the path to the file which contains the execution data. -->
							<destFile>${project.build.directory}/jacoco.exec</destFile>
							<!-- Exclusion des codes générés et des loaders ayant des méthodes
                                trop longues -->
							<excludes>
								<exclude>**/generated/**</exclude>
								<exclude>**/generated/*</exclude>
								<exclude>**/domain/model/id/**</exclude>
								<exclude>**/domain/data/**</exclude>
								<exclude>**/infrastructure/configuration/**</exclude>
								<exclude>**/infrastructure/jpa/entity/**</exclude>
								<exclude>**/infrastructure/mapper/**</exclude>
								<exclude>**/infrastructure/restapi/dto/**</exclude>
							</excludes>
							<propertyName>surefireArgLine</propertyName>
						</configuration>
					</execution>
					<!-- Reporting TUs -->
					<execution>
						<id>post-unit-test</id>
						<phase>verify</phase>
						<goals>
							<goal>report</goal>
						</goals>
						<configuration>
							<!-- Sets the path to the file which contains the execution data. -->
							<dataFile>${project.build.directory}/jacoco.exec</dataFile>
							<!-- Sets the output directory for the code coverage report. -->
							<outputDirectory>${project.build.directory}/jacoco</outputDirectory>
							<!-- Exclusion des codes générés -->
							<excludes>
								<exclude>**/generated/**</exclude>
								<exclude>**/generated/*</exclude>
								<exclude>**/domain/model/id/**</exclude>
								<exclude>**/domain/data/**</exclude>
								<exclude>**/infrastructure/configuration/**</exclude>
								<exclude>**/infrastructure/jpa/entity/**</exclude>
								<exclude>**/infrastructure/mapper/**</exclude>
								<exclude>**/infrastructure/restapi/dto/**</exclude>
							</excludes>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>

	</build>
</project>
